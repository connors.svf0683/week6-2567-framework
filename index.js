const http = require('http')

const PORT = 3000
// console.log(http.METHODS)

// console.log(http.STATUS_CODES)

const app = http.createServer((req,res) => {

    if (req.url === '/') {

        res.writeHead(200,{"Content-Type":"text/html"})
        res.write("<h3 style='color:red'>Welcome to NodeJS and express framework!!!</h3>")
        res.end()

    }

    else if (req.url === '/about') {

        res.writeHead(200,{"Content-Type":"text/plain"})
        res.write("<h3>About Page</h3>")
        res.end()

    }

    else {

        res.writeHead(200,{"Content-Type":"text/html"})
        res.write("<h1>Not Found!!!</h1>")
        res.end()

    }
    
})

app.listen(PORT, () => console.log(`Server running at http://localhost:${PORT}`))